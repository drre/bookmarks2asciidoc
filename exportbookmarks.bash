#!/bin/bash

# exports a bookmark hierarchy to asciidoc
#
# call with: bookmarks2adoc YOUR_BOOKMARK_FOLDER
#
# Requirements: saxonb-xslt, sqlite3,
#
# folders become sections, bookmarks become links in a list
#
# Note. asciidoc only supports section up to depth 4,
# so bookmarks at a lower level will generate invalid section titles

# Note. Basically all of this is taken from searches on google and
# stackoverflow. Errors are mine, though.

# Copyright 2016 Ruben Real (real.ruben@gmail.com)
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.


# --------- NO CHANGES BELOW THIS LINE
OLDIFS=$IFS
IFS="^"
MYDIR="$(dirname "$(readlink -f "$0")")"

function sql_bookmarks {
# get a hierarchy of bookmarks in csv format

# using a recursive CTE search for all childs belonging to a parent. 
# the parent is identified by the global variable $1 (see call to sql_bookmarks)

sqlite3 $PLACES  << EOF
.mode csv
.header on
.separator $IFS

WITH RECURSIVE
	child_of(id,level) AS (
		VALUES($1,0)
		UNION
		SELECT moz_bookmarks.id, child_of.level + 1
		FROM moz_bookmarks
		join
			child_of on moz_bookmarks.parent = child_of.id
		order by 2 desc
		)
	select child_of.id, type, level, fk, parent, moz_bookmarks.title, url
	from
		child_of join moz_bookmarks using(id)
	left join moz_places on moz_places.id = fk ;
.exit
EOF
}


function toxml() {
	# convert hierarchical csv to xml

    gawk --field-separator '^' --file "$MYDIR/toxml.awk"

}

function fixcsv () {
	# strip column names and remove windows-style CR
	tail -n +2  | sed -e 's/\r//g'
}


# finally, run this

POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
	-a|--adoc)
	    XSLTTRANSFORM="$MYDIR/adoc.xslt"
	    FOLDER="$2"
	    shift # past argument
	    shift # past value
	    ;;
	-o|--orgmode)
	    XSLTTRANSFORM="$MYDIR/orgmode.xslt"
	    FOLDER="$2"
	    shift # past argument
	    shift # past value
	    ;;
	-p|--places)
	    PLACES="$2"
	    shift
	    shift
	    ;;
	*)    # unknown option
	    POSITIONAL+=("$1") # save it in an array for later
	    shift # past argument
	    ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

echo "${Folder}"
# get id of folder of interest
ID=`sqlite3 ${PLACES} "SELECT id from moz_bookmarks where lower(title) LIKE \"${FOLDER}\""`
[ -z "$ID"  ] && { echo "$2 not found"; exit 99; }



bookmarktype=`sqlite3 ${PLACES} "SELECT type from moz_bookmarks where id=$ID"`
[ "$bookmarktype" -ne 2  ] && { echo "${FOLDER} is not a folder"; exit 99; }


# export xsl-transform to tmpfile
sql_bookmarks $ID | fixcsv  | toxml   | saxonb-xslt -s:- $XSLTTRANSFORM

