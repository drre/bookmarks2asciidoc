<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
  <xsl:output method="text" indent="yes" omit-xml-declaration="yes"/>
  <xsl:template match="/folders">
    <xsl:apply-templates select="folder"/>
  </xsl:template>
  <xsl:template match="folder">
    <xsl:sequence select="string-join (('&#10;&#10;', for $i in (1 to count (ancestor::folder) + 2) return '='),'')"/>
    <xsl:value-of select="title"/>
    <xsl:text>&#xD;</xsl:text>
    <xsl:text>&#xD;</xsl:text>
    <xsl:apply-templates select="folder"/>
    <xsl:apply-templates select="bookmark"/>
  </xsl:template>
  <xsl:template match="bookmark">
    * link:$$<xsl:value-of select="@href" disable-output-escaping="no"/>$$[<xsl:value-of select="title"/>]
    
  </xsl:template>
</xsl:stylesheet>
